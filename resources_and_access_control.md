
# Creating an API that supports different use cases

One of the benfits of API First is the possibility to scale the number of Consumers without affecting the number of people working with providing the API. A good Developer Portal is an important ingredient in this.

But enabling scaling the number of Consumers without increasing the people on the Provider side is also about API design. One API that can be used in many use cases is better than providing a new API for each use case. This is of course true only if the one API has one well defined responsibility and is easy to understand. Combining unrelated features into the same API will have negative impacts on both Provider and Consumers. 

Separate what a User requests to do (URL and request body) from what that User is permitted to do opens up for more Consumers. 

Example:

Consumer need: A mobile banking app needs an API to get the accounts of the bank customer.


## API Design alternative 1 (naive approach)

    Request
    GET https://example.com/bank-accounts

    Authorization: {userId: customerA} // customerA authenticated in some way


    Response
    200 OK

    {
        "value": [
            {
                "bankAccountNumber": "123456789",
                "accountOwner": "customerA",
                "balance": "501.00"					
            },
	        {
                "bankAccountNumber": "1212121212",
                "customerId": "customerA",
                "balance": "1000.00"					
            },
	    // more items omitted for readability
        ]
    }

The API uses the authenticated userId and filters all Bank Account Items with accountOwner: "customerA"

This API fulfills the identified Consumer need, but what happens when new needs are identified?

New Consumer need: A banking officer needs access to one customer's bank accounts

Let's see what happens if a bank officer calls the API.


    Request
    GET https://example.com/bank-accounts

    Authorization: {userId: bankOfficer1} // customerA authenticated in some way

    Response
    200 OK
    
    {
        "value": []	
    }


The API is not able to fulfill the new need. This is because the authenticated userId is used for determining which bank accounts to filter.

The API retuns all accounts of customerId: "bankOfficer1" which is not what is wanted! A URL should uniquely identify the Resource, but this API design have one URL for many Resources.

## Design alternative 2 - Supporting many use cases  


    
    Request
    GET https://example.com/bank-accounts?filter.accountOwner=customerA

    Authorization: {userId: bankOfficer1} // customerA authenticated in some way
    
    
    Response

    {
        "value": [
            {
                "bankAccountNumber": "123456789",
                "accountOwner": "customerA",
                "balance": "501.00"					
            },
	        {
                "bankAccountNumber": "1212121212",
                "accountOwner": "customerA",
                "balance": "1000.00"					
            },
	        // more items omitted for readability
        ]
    }

By separating what the user wants (the Resource identified by the URL) and who the user is (in Authorization header) the Resource is valid for different use cases.